## **Online market**
You're training to either a Back-end or Front-end developer position at Netcracker, and in this project it is require to accomplish 20-40 hours per week during 2 month working in a 2 persons team.

**Here we're trying to assess the following skills:**

*	Understanding and solving of a given problem.
*	Comprehensible code writing and organization.
*	Knowledge of Java and Angular.
*	Use of Java runtime and its libraries.
*	Intelligible and useful documentation writing, like READMEs and code comments.
*	Development practices like automated tests.
*	Comprehensible English writing.
*	Understanding of microservices architecture
*	Knowledge of RabbitMQ, Reddis and Docker
*	Understanding if Spring WebFlux and Reactive Streams

You can submit answers on a GitHub repository or gist, or by email if you require privacy or confidentiality. We'd like all documentation to be written in English.

---
**It this project you will be able to get knowledge of:**

*	Creating a project from scratch.
*	Organizing and splitting requirements in a work team.
*	Use of Git repository.
*	Build Java microservices using Spring.
*	Dependency management with Maven.
*	Create REST APIs.
*	Exceptions and errors management.
*	Create and manage I/O transactions in a database.
*	Create unit test.

---
## **Requirements**

*	User should be able to create an order and add products
*	For each order should exist unique order id
*	User should be able to continue his order using order id
*	Orders should be able to be in 2 statuses: In Progress and Processed
*	User should be able to 'buy' his filled order
*	During buying, user should see the following information about the order: list of products with prices, total price
*	During buying, user should be able to configure number products
*	During buying, user should be able to remove products from the list
*	System should store all In Progress orders for 24 hours after last update
*	System should store all Processed orders for 1 month